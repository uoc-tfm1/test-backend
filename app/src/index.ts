// Copyright (c) 2021 Irian Montón Beltrán
//
// This Source Code Form is subject to the terms of the Mozilla Public
// License, v. 2.0. If a copy of the MPL was not distributed with this
// file, You can obtain one at http://mozilla.org/MPL/2.0/.

import {createApp} from './app';
import {startListeningToPort} from './server';

require('dotenv').config();

const app = createApp();
startListeningToPort(app);
