// Copyright (c) 2021 Irian Montón Beltrán
//
// This Source Code Form is subject to the terms of the Mozilla Public
// License, v. 2.0. If a copy of the MPL was not distributed with this
// file, You can obtain one at http://mozilla.org/MPL/2.0/.

import express = require('express');
import ItemsCollection from './models/database/ItemsCollection';
import OrdersCollection from './models/database/OrdersCollection';
import {
  FirebaseUser,
  StaffUser,
} from './models/database/types/FirebaseUser';
import Item from './models/database/types/Item';
import {orderPossibleStatuses} from './models/database/types/OrderStatus';
import UsersCollection from './models/database/UsersCollection';
import FirebaseService from './services/FirebaseService';

export const createApp = (): express.Application => {
  const app = express();

  const firebaseService = new FirebaseService();
  const itemsCollection = new ItemsCollection(firebaseService.firestore);
  const usersCollection = new UsersCollection(
    firebaseService.firestore,
    firebaseService.messaging,
  );
  const ordersCollection = new OrdersCollection(
    firebaseService.firestore,
    usersCollection,
  );

  app.use((req, res, next) => {
    console.log('Request incoming: ', {
      ip: req.ip,
      endpoint: req.url,
      method: req.method,
      headers: req.headers,
    });

    next();
  });

  // Enable Express to parse JSON payloads.
  app.use(express.json());

  app.get('/', (req, res) => {
    res.send(
      wrapResultAsSuccess(
        `Hello! This is Irian Montón's UOC TFM quick API.`,
      ),
    );
  });

  // ### ITEMS

  app.get('/items', async (req, res) => {
    const items: Item[] = await itemsCollection.getAllItems();

    res.send(
      wrapResultAsSuccess(items.map((item) => item.serializeAsJS())),
    );
  });

  app.get('/items/:id', async (req, res) => {
    const item: Item | null = await itemsCollection.getSingleItem(
      req.params.id,
    );

    if (item == null) {
      res.status(404).send(wrapResultAsError(`NOT FOUND`));
    } else {
      res.send(wrapResultAsSuccess(item.serializeAsJS()));
    }
  });

  // ### USERS

  app.get('/users', async (req, res) => {
    const users: FirebaseUser[] = await usersCollection.getAllUsers();

    res.send(
      wrapResultAsSuccess(users.map((user) => user.serializeAsJS())),
    );
  });

  app.get('/users/:id', async (req, res) => {
    const user: FirebaseUser | null =
      await usersCollection.getSingleUserById(req.params.id);

    if (user == null) {
      res.status(404).send(wrapResultAsError(`NOT FOUND`));
    } else {
      res.send(wrapResultAsSuccess(user.serializeAsJS()));
    }
  });

  app.get('/users/:id/is-available', async (req, res) => {
    const userId = req.params.id;

    const user: FirebaseUser | null =
      await usersCollection.getSingleUserById(userId);

    if (user == null) {
      res.status(404).send(wrapResultAsError('USER NOT FOUND'));
    } else if (!StaffUser.isOfType(user)) {
      res.status(404).send(wrapResultAsError('User is not staff'));
    } else {
      res.send(wrapResultAsSuccess(user.isAvailable));
    }
  });

  app.post('/users', async (req, res) => {
    if (FirebaseUser.isOfType(req.body)) {
      await usersCollection.addUpdateUser(req.body);
      res.send(wrapResultAsSuccess('User saved!'));
    } else {
      res.status(400).send(wrapResultAsError('Invalid object format'));
    }
  });

  // Send with query param 'id=' for user Id
  app.post('/users/fcm-token', async (req, res) => {
    const userId = req.query.id?.toString();
    const token = req.body.data;

    if (userId == null) {
      res
        .status(400)
        .send(wrapResultAsError('Missing user id query variable'));

      return;
    }

    if (typeof token === 'string' && token.length > 3) {
      await usersCollection.setFcmToken(userId, token);
      res.send(wrapResultAsSuccess('User saved!'));
    } else {
      res.status(400).send(wrapResultAsError('Invalid token format'));
    }
  });

  app.post('/users/:id/is-available', async (req, res) => {
    const userId = req.params.id;
    const newValue = req.query.value?.toString();

    if (newValue === 'true' || newValue === 'false') {
      const user: FirebaseUser | null =
        await usersCollection.getSingleUserById(userId);

      if (user == null) {
        res.status(404).send(wrapResultAsError('USER NOT FOUND'));
      } else if (!StaffUser.isOfType(user)) {
        res.status(404).send(wrapResultAsError('User is not staff'));
      } else {
        user.isAvailable = Boolean(newValue === 'true');
        await usersCollection.addUpdateUser(user);

        res.send(wrapResultAsSuccess(user.isAvailable));
      }
    } else {
      res
        .status(400)
        .send(
          wrapResultAsError("Invalid value for 'newValue' query param."),
        );
    }
  });

  // ### ORDERS

  app.get('/orders', async (req, res) => {
    const orders = await ordersCollection.getAllOrders();

    res.send(
      wrapResultAsSuccess(orders.map((order) => order.serializeAsJS())),
    );
  });

  app.get('/orders/:id', async (req, res) => {
    const order = await ordersCollection.getSingleOrderById(req.params.id);

    if (order == null) {
      res.status(404).send(wrapResultAsError(`NOT FOUND`));
    } else {
      res.send(wrapResultAsSuccess(order.serializeAsJS()));
    }
  });

  app.post('/orders', async (req, res) => {
    const newOrderData = req.body;

    // Loosely checking type
    if (
      newOrderData.items != null &&
      newOrderData.user != null &&
      newOrderData.amount != null
    ) {
      const newOrderResult = await ordersCollection.createNewOrder(
        usersCollection,
        newOrderData,
      );

      if (newOrderResult.order != null) {
        // Simulating auto confirmation of order
        setTimeout(() => {
          ordersCollection.updateOrderStatus(
            newOrderResult.order!.uuid,
            'planned',
          );
        }, 5000);

        res.send(wrapResultAsSuccess(newOrderResult.order));
      } else {
        res
          .status(500)
          .send(
            wrapResultAsError(
              newOrderResult.error ?? 'Unespecified error',
            ),
          );
      }
    } else {
      res.status(400).send(wrapResultAsError('Invalid order format'));
    }
  });

  app.put('/orders/:id/status', async (req, res) => {
    // Variable checking
    const newStatus: any = req.body?.data;

    if (!orderPossibleStatuses.includes(newStatus)) {
      res.status(400).send(wrapResultAsError('Invalid status value'));
      return;
    }

    const order = await ordersCollection.getSingleOrderById(req.params.id);

    if (order == null) {
      res.status(404).send(wrapResultAsError('Order not found'));
      return;
    }

    // If all variables are correct we proceed to add a new status to the order.
    const updatedOrder = await ordersCollection.updateOrderStatus(
      order.uuid,
      newStatus,
    );

    if (updatedOrder != null) {
      res.send(wrapResultAsSuccess(updatedOrder.serializeAsJS()));
    } else {
      res
        .status(500)
        .send(wrapResultAsError("Couldn't update status due to an error"));
    }
  });

  // ### DEBUG

  if (process.env.NODE_ENV === 'development') {
    app.get('/debug', async (req, res) => {
      res.send(wrapResultAsSuccess('Development mode is on!'));
    });

    app.get('/debug/reset-staff-users', async (req, res) => {
      // await itemsCollection.DEBUG_emptyAndPopulateDbWithDefaultData();
      await usersCollection.DEBUG_DeleteStaffAndRepopulateDB(
        'by2i1JrIqRTTHam1aZbouN4M55h2',
        'zcfRQFsJK5P8aDWU2tm71ksP1BB2',
      );

      res.send(wrapResultAsSuccess('Done!'));
    });

    app.get('/debug/cancel-order', async (req, res) => {
      const orderId = req.query.id?.toString();

      if (orderId != null) {
        await ordersCollection.updateOrderStatus(orderId, 'cancelled');
        return res.send('Done!');
      } else {
        res.status(404).send(`Order ID ${orderId} not found`);
      }

      res.send(wrapResultAsSuccess('Done!'));
    });
  }

  return app;
};

function wrapResultAsSuccess<T>(data: T): {data: T} {
  return {
    data: data,
  };
}

function wrapResultAsError<T>(data: T): {error: T} {
  return {
    error: data,
  };
}
