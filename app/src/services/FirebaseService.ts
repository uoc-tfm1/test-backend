// Copyright (c) 2021 Irian Montón Beltrán
//
// This Source Code Form is subject to the terms of the Mozilla Public
// License, v. 2.0. If a copy of the MPL was not distributed with this
// file, You can obtain one at http://mozilla.org/MPL/2.0/.

import {applicationDefault, initializeApp} from 'firebase-admin/app';
import {getFirestore} from 'firebase-admin/firestore';
import {getMessaging} from 'firebase-admin/messaging';

class FirebaseService {
  readonly firestore;
  readonly messaging;

  constructor() {
    this.initialize();
    this.firestore = getFirestore();
    this.firestore.settings({ignoreUndefinedProperties: true});

    this.messaging = getMessaging();
  }

  private initialize() {
    initializeApp({
      // For this to work you need to set up GOOGLE_APPLICATION_CREDENTIALS environment variable.
      credential: applicationDefault(),
    });
  }
}

export default FirebaseService;
