// Copyright (c) 2021 Irian Montón Beltrán
//
// This Source Code Form is subject to the terms of the Mozilla Public
// License, v. 2.0. If a copy of the MPL was not distributed with this
// file, You can obtain one at http://mozilla.org/MPL/2.0/.

import {v4 as uuidv4} from 'uuid';
import Order, {IOrder} from './types/Order';
import {OrderStatus} from './types/OrderStatus';
import UsersCollection from './UsersCollection';

class OrdersCollection {
  readonly collection;
  readonly usersCollection;

  constructor(
    firestore: FirebaseFirestore.Firestore,
    usersCollection: UsersCollection,
  ) {
    this.collection = firestore.collection('orders');
    this.usersCollection = usersCollection;
  }

  async getAllOrders(): Promise<Order[]> {
    const docRefs = await this.collection.listDocuments();

    const orders: Order[] = await (async () => {
      const docs = (
        await Promise.all(docRefs.map((ref) => ref.get()))
      ).filter((doc) => doc.exists);

      const ordersObjs = docs
        .map((doc) => doc.data())
        .filter((data) => data != null);

      return ordersObjs.map((obj: IOrder) => new Order(obj));
    })();

    return orders;
  }

  async getSingleOrderById(uuid: string): Promise<Order | null> {
    const orderObj = (await this.collection.doc(uuid).get()).data();

    if (orderObj == null) {
      return null;
    } else {
      return new Order(orderObj as IOrder);
    }
  }

  async createNewOrder(
    usersCollection: UsersCollection,
    newOrderData: Partial<IOrder>,
  ): Promise<{order: Order | null; error: string | null}> {
    const freeStaffUser = await usersCollection.getFreeStaffUser();

    if (freeStaffUser == null) {
      return {order: null, error: 'no-free-staff-available'};
    }

    const newId = uuidv4();
    const assignedStaffResult =
      await usersCollection.assignOrdersToStaffUser(freeStaffUser!.uuid, [
        newId,
      ]);

    if (assignedStaffResult.error != null) {
      return {order: null, error: assignedStaffResult.error};
    }

    // Cleaning user from its fcmTokens since it should never be accessed from the order because they expire easily
    const user = newOrderData.user!;

    if (user.fcmToken != null) {
      delete user.fcmToken;
    }

    const newOrder: IOrder = {
      uuid: newId,
      kind: newOrderData.kind!,
      user: newOrderData.user!,
      staff: assignedStaffResult.user!.serializeAsJS(),
      deliveryPlace: newOrderData.deliveryPlace,
      time: newOrderData.time,
      pickUpPlace: newOrderData.pickUpPlace!,
      orderStatus: newOrderData.orderStatus ?? [
        {status: 'pending', timestamp: new Date().toISOString()},
      ],
      amount: newOrderData.amount!,
      items: newOrderData.items!,
      dueTime:
        // The time plus 30 minutes to give time to a worker to arrive.
        newOrderData.dueTime ??
        new Date(Date.now() + 30 * 60 * 1000).toISOString(),
      comment: newOrderData.comment,
    };

    await this.addUpdateOrder(newOrder);

    return {order: new Order(newOrder), error: null};
  }

  async addUpdateOrder(order: IOrder): Promise<void> {
    if (order instanceof Order) {
      await this.collection.doc(order.uuid).set(order.serializeAsJS());
    } else {
      await this.collection.doc(order.uuid).set(order);
    }
  }

  async removeOrder(uuid: string): Promise<void> {
    const ref = this.collection.doc(uuid);

    if (!(await ref.get()).exists) {
      return;
    } else {
      await ref.delete();
    }
  }

  async updateOrderStatus(
    uuid: string,
    newStatus: OrderStatus['status'],
  ): Promise<Order | null> {
    const order = await this.getSingleOrderById(uuid);

    if (order == null) {
      return order;
    }

    order.orderStatus.push({
      status: newStatus,
      timestamp: new Date().toISOString(),
    });

    await this.addUpdateOrder(order);

    let notificationMessage: string | undefined;

    switch (newStatus) {
      case 'planned':
        notificationMessage =
          'Su servicio ha sido confirmado, le esperamos puntual en el punto de recogida.';
        break;

      case 'on_route':
        notificationMessage =
          'Su conductor ha salido a recogerle, por favor espere en el punto de recogida.';
        break;

      case 'completed':
        notificationMessage =
          'Su servicio ha sido completado, muchas gracias por confiar en nosotros.';
        break;

      case 'cancelled':
        notificationMessage =
          'Su servicio ha sido cancelado, contacte con Assistant Drivers para más detalles.';
        break;

      case 'failed':
        notificationMessage =
          'Ha ocurrido un error con su servicio, contacte con Assistant Drivers para más detalles.';
        break;

      case 'pending':
      case 'payment_pending':
      default:
        break;
    }

    if (notificationMessage != null) {
      await this.usersCollection.sendNotificationToUser(order.user.uuid, {
        title: 'Actualización de servicio',
        message: notificationMessage,
      });
    }

    return order;
  }
}

export default OrdersCollection;
