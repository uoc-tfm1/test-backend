// Copyright (c) 2021 Irian Montón Beltrán
//
// This Source Code Form is subject to the terms of the Mozilla Public
// License, v. 2.0. If a copy of the MPL was not distributed with this
// file, You can obtain one at http://mozilla.org/MPL/2.0/.

import {Messaging} from 'firebase-admin/messaging';
import {
  FirebaseUser,
  IFirebaseUser,
  PlainUser,
  StaffUser,
} from './types/FirebaseUser';

class UsersCollection {
  readonly collection;
  readonly notificationService;

  constructor(
    firestore: FirebaseFirestore.Firestore,
    notificationService: Messaging,
  ) {
    this.collection = firestore.collection('users');
    this.notificationService = notificationService;
  }

  async getAllUsers(): Promise<FirebaseUser[]> {
    const docRefs = await this.collection.listDocuments();

    const users = await (async () => {
      const docs = (
        await Promise.all(docRefs.map((ref) => ref.get()))
      ).filter((doc) => doc.exists);

      const usersObjs = docs
        .map((doc) => doc.data())
        .filter((data) => data != null);

      return usersObjs.map((obj) => {
        if (PlainUser.isOfType(obj)) {
          return new PlainUser(obj);
        } else if (StaffUser.isOfType(obj)) {
          return new StaffUser(obj);
        } else {
          return new FirebaseUser(obj as IFirebaseUser);
        }
      });
    })();

    return users;
  }

  async getSingleUserById(uuid: string): Promise<FirebaseUser | null> {
    const userObj = (await this.collection.doc(uuid).get()).data();

    if (userObj == null) {
      return null;
    } else {
      if (StaffUser.isOfType(userObj)) {
        return new StaffUser(userObj);
      } else if (PlainUser.isOfType(userObj)) {
        return new PlainUser(userObj);
      } else {
        return new FirebaseUser(userObj as IFirebaseUser);
      }
    }
  }

  async getFreeStaffUser(): Promise<StaffUser | null> {
    const users = (await this.getAllUsers()).filter(
      (user) => user instanceof StaffUser,
    ) as StaffUser[];

    return users.find((user) => user.isAvailable) ?? null;
  }

  async assignOrdersToStaffUser(
    userId: string,
    orderIds: string[],
  ): Promise<{user: StaffUser | null; error: string | null}> {
    const user = await this.getSingleUserById(userId);

    if (user == null) {
      return {user: null, error: 'not-found'};
    }

    if (user.role !== 'staff') {
      return {user: null, error: 'incorrect-role'};
    }

    const staffUser = user as StaffUser;
    staffUser.assignOrders(orderIds);
    await this.addUpdateUser(staffUser);

    return {user: staffUser, error: null};
  }

  async addUpdateUser<T extends IFirebaseUser>(user: T): Promise<void> {
    if (user instanceof FirebaseUser) {
      await this.collection.doc(user.uuid).set(user.serializeAsJS());
    } else {
      await this.collection.doc(user.uuid).set(user);
    }
  }

  async removeUser(uuid: string): Promise<void> {
    const ref = this.collection.doc(uuid);

    if (!(await ref.get()).exists) {
      return;
    } else {
      await ref.delete();
    }
  }

  async setFcmToken(userId: string, token: string) {
    const user = await this.getSingleUserById(userId);

    if (user != null) {
      user.fcmToken = token;
      this.addUpdateUser(user);
    }
  }

  async sendNotificationToUser(
    uuid: string,
    notification: {title: string; message: string},
  ): Promise<void> {
    const user = await this.getSingleUserById(uuid);

    if (user == null) {
      console.error(
        'UsersCollection.sendNotificationToUser',
        'Missing user',
      );
      return;
    }

    if (user.fcmToken == null) {
      console.error(
        'UsersCollection.sendNotificationToUser',
        'Missing user token',
      );
      return;
    }

    const fcmNotification = {
      notification: {
        title: notification.title,
        body: notification.message,
      },
      token: user.fcmToken,
    };

    await this.notificationService
      .send(fcmNotification)
      .then((response) => {
        // Response is a message ID string.
        console.log(
          `Successfully sent notification to token ${fcmNotification.token} . Message response:`,
          response,
        );
      })
      .catch((error) => {
        console.error(
          'UsersCollection.sendNotificationToUser; Error sending notification: ',
          error,
        );
      });
  }

  async DEBUG_DeleteStaffAndRepopulateDB(
    firebaseId1: string,
    firebaseId2: string,
  ) {
    const newStaff = [
      new StaffUser({
        uuid: firebaseId1,
        email: 'staff1@test.com',
        isAvailable: true,
        name: 'Yocorro Mucho',
        ordersAssignedIds: [],
        phone: '000000000',
        role: 'staff',
      }),
      new StaffUser({
        uuid: firebaseId2,
        email: 'staff2@test.com',
        isAvailable: true,
        name: 'Quemel Apego',
        ordersAssignedIds: [],
        phone: '000000001',
        role: 'staff',
      }),
    ];

    // Deleting existing staff users
    const existingStaffUsersIds = (await this.getAllUsers())
      .filter((user) => user.role === 'staff')
      .map((user) => user.uuid);

    await Promise.all(
      existingStaffUsersIds.map((id) => this.collection.doc(id).delete()),
    );

    // Adding new staff users
    await Promise.all(
      newStaff.map((staff) => {
        return this.collection.doc(staff.uuid).set(staff.serializeAsJS());
      }),
    );
  }
}

export default UsersCollection;
