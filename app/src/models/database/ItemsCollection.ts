// Copyright (c) 2021 Irian Montón Beltrán
//
// This Source Code Form is subject to the terms of the Mozilla Public
// License, v. 2.0. If a copy of the MPL was not distributed with this
// file, You can obtain one at http://mozilla.org/MPL/2.0/.

import Item, {IItem} from './types/Item';
import {DEBUG_emptyCollection} from './Utils';

class ItemsCollection {
  readonly collection;

  constructor(firestore: FirebaseFirestore.Firestore) {
    this.collection = firestore.collection('items');
  }

  async getAllItems(): Promise<Item[]> {
    const docRefs = await this.collection.listDocuments();

    const items: Item[] = await (async () => {
      const docs = (
        await Promise.all(docRefs.map((ref) => ref.get()))
      ).filter((doc) => doc.exists);

      const itemsObjs = docs
        .map((doc) => doc.data())
        .filter((data) => data != null);

      return itemsObjs.map((obj: IItem) => new Item(obj));
    })();

    return items;
  }

  async getSingleItem(id: string): Promise<Item | null> {
    const itemObj = (await this.collection.doc(id).get()).data();

    if (itemObj == null) {
      return null;
    } else {
      return new Item(itemObj as IItem);
    }
  }

  private async DEBUG_populateDb() {
    const items = [
      new Item({
        name: 'Electric',
        imageURL:
          'https://firebasestorage.googleapis.com/v0/b/uoc-tfm-331322.appspot.com/o/items%2Fimages%2FCoche%20Inifinity.png?alt=media&token=25355919-5ee5-4680-b1b6-614c266f0c12',
        prices: {
          base: 10 * 100,
          perKm: 4 * 100,
          perHour: 20 * 100,
        },
      }),
      new Item({
        name: 'Executive',
        imageURL:
          'https://firebasestorage.googleapis.com/v0/b/uoc-tfm-331322.appspot.com/o/items%2Fimages%2FCoche%20mercedes.png?alt=media&token=a440764d-50b2-40f5-8b29-d530d28c9411',
        prices: {
          base: 20 * 100,
          perKm: 6 * 100,
          perHour: 30 * 100,
        },
      }),
      new Item({
        name: 'Furgoneta',
        imageURL:
          'https://firebasestorage.googleapis.com/v0/b/uoc-tfm-331322.appspot.com/o/items%2Fimages%2FCoche%20furgoneta.png?alt=media&token=846abc84-ce56-4a98-a0dc-99d10a84bbc3',
        prices: {
          base: 10 * 100,
          perKm: 8 * 100,
          perHour: 20 * 100,
        },
      }),
    ];

    const promises = items.map((item) => {
      return this.collection.doc(item.uuid).set(item.serializeAsJS());
    });

    await Promise.all(promises);
  }

  async DEBUG_emptyAndPopulateDbWithDefaultData() {
    await DEBUG_emptyCollection(this.collection);
    await this.DEBUG_populateDb();
  }
}

export default ItemsCollection;
