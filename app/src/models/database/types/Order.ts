// Copyright (c) 2021 Irian Montón Beltrán
//
// This Source Code Form is subject to the terms of the Mozilla Public
// License, v. 2.0. If a copy of the MPL was not distributed with this
// file, You can obtain one at http://mozilla.org/MPL/2.0/.

// Copyright (c) 2021 Irian Montón Beltrán
//
// This Source Code Form is subject to the terms of the Mozilla Public
// License, v. 2.0. If a copy of the MPL was not distributed with this
// file, You can obtain one at http://mozilla.org/MPL/2.0/.

import {v4 as uuidv4} from 'uuid';
import {IPlainUser, IStaffUser} from './FirebaseUser';
import {IItem} from './Item';
import {Location} from './Location';
import {OrderStatus} from './OrderStatus';

export interface IOrder {
  uuid: string;
  kind: 'distance' | 'time';
  user: IPlainUser;
  staff: IStaffUser;
  // TODO Change Location to Place object like in the app
  deliveryPlace?: Location;
  /**
   * Time in hours.
   */
  time?: number;
  pickUpPlace: Location;
  orderStatus: OrderStatus[];
  items: IItem[];
  /**
   * Total amount charged to the user, in cents and in euros.
   */
  amount: number;
  /**
   * Time when the order should be delivered to the client.
   */
  dueTime: string;
  comment?: string;
  // TODO Add createdAt
}

class Order implements IOrder {
  uuid: string;
  kind: 'distance' | 'time';
  user: IPlainUser;
  staff: IStaffUser;
  deliveryPlace?: Location | undefined;
  time?: number | undefined;
  pickUpPlace: Location;
  orderStatus: OrderStatus[];
  items: IItem[];
  amount: number;
  dueTime: string;
  comment?: string | undefined;

  constructor(order: Omit<IOrder, 'uuid'> & {uuid?: string}) {
    this.uuid = order.uuid || uuidv4();
    this.kind = order.kind;
    this.user = order.user;
    this.staff = order.staff;
    this.deliveryPlace = order.deliveryPlace;
    this.time = order.time;
    this.pickUpPlace = order.pickUpPlace;
    this.orderStatus = order.orderStatus;
    this.items = order.items;
    this.amount = order.amount;
    this.dueTime = order.dueTime;
    this.comment = order.comment;
  }

  serializeAsJS(): IOrder {
    const obj: IOrder = {
      uuid: this.uuid,
      kind: this.kind,
      user: this.user,
      staff: this.staff,
      deliveryPlace: this.deliveryPlace,
      time: this.time,
      pickUpPlace: this.pickUpPlace,
      orderStatus: this.orderStatus,
      items: this.items,
      amount: this.amount,
      dueTime: this.dueTime,
      comment: this.comment,
    };

    return obj;
  }
}

export default Order;
