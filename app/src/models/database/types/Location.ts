// Copyright (c) 2021 Irian Montón Beltrán
//
// This Source Code Form is subject to the terms of the Mozilla Public
// License, v. 2.0. If a copy of the MPL was not distributed with this
// file, You can obtain one at http://mozilla.org/MPL/2.0/.

export interface Location {
  coordinates: {lat: number; lng: number};
  postalCode?: string;
  address: string;
  addressDetails: string;
}
