// Copyright (c) 2021 Irian Montón Beltrán
//
// This Source Code Form is subject to the terms of the Mozilla Public
// License, v. 2.0. If a copy of the MPL was not distributed with this
// file, You can obtain one at http://mozilla.org/MPL/2.0/.

import {v4 as uuidv4} from 'uuid';

export interface IItem {
  uuid: string;
  name: string;
  imageURL: string;
  /**
   * Prices in cents and in euros.
   */
  prices: {
    /**
     * Minimum price.
     */
    base: number;
    perKm: number;
    perHour: number;
  };
}

class Item implements IItem {
  uuid: string;
  name: string;
  imageURL: string;
  prices: {
    base: number;
    perKm: number;
    perHour: number;
  };

  constructor({
    uuid,
    name,
    imageURL,
    prices,
  }: {
    uuid?: string;
    name: string;
    imageURL: string;
    /**
     * Prices in cents and in euros.
     */
    prices: {
      /**
       * Minimum price.
       */
      base: number;
      perKm: number;
      perHour: number;
    };
  }) {
    this.uuid = uuid ?? uuidv4();
    this.name = name;
    this.imageURL = imageURL;
    this.prices = prices;
  }

  serializeAsJS(): IItem {
    const obj: IItem = {
      uuid: this.uuid,
      name: this.name,
      imageURL: this.imageURL,
      prices: this.prices,
    };

    return obj;
  }
}

export default Item;
