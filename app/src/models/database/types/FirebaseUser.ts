// Copyright (c) 2021 Irian Montón Beltrán
//
// This Source Code Form is subject to the terms of the Mozilla Public
// License, v. 2.0. If a copy of the MPL was not distributed with this
// file, You can obtain one at http://mozilla.org/MPL/2.0/.

import {v4 as uuidv4} from 'uuid';

export interface IFirebaseUser {
  /**
   * The same Firebase user ID.
   */
  uuid: string;
  name: string;
  email: string;
  phone: string;
  fcmToken?: string;
  role: 'staff' | 'user' | 'admin' | 'debug';
}

export interface IPlainUser extends IFirebaseUser {
  role: 'user';
}

export interface IStaffUser extends IFirebaseUser {
  role: 'staff';
  isAvailable: boolean;
  ordersAssignedIds: string[];
}

export class FirebaseUser implements IFirebaseUser {
  uuid: string;
  name: string;
  email: string;
  phone: string;
  fcmToken?: string;
  role: 'staff' | 'user' | 'admin' | 'debug';

  constructor(user: Omit<IFirebaseUser, 'uuid'> & {uuid?: string}) {
    this.uuid = user.uuid ?? uuidv4();
    this.name = user.name;
    this.email = user.email;
    this.phone = user.phone;
    this.fcmToken = user.fcmToken;
    this.role = user.role;
  }

  static isOfType(obj: any): obj is IFirebaseUser {
    return (
      obj != null &&
      typeof obj.uuid === 'string' &&
      typeof obj.name === 'string' &&
      typeof obj.email === 'string' &&
      typeof obj.phone === 'string' &&
      typeof obj.role === 'string'
    );
  }

  serializeAsJS(): IFirebaseUser {
    const obj: IFirebaseUser = {
      uuid: this.uuid,
      name: this.name,
      email: this.email,
      phone: this.phone,
      fcmToken: this.fcmToken,
      role: this.role,
    };

    return obj;
  }
}

export class PlainUser extends FirebaseUser implements IPlainUser {
  role: 'user' = 'user';

  constructor(user: Omit<IPlainUser, 'uuid'> & {uuid?: string}) {
    super(user);
  }

  static isOfType(obj: any): obj is IPlainUser {
    return (
      obj != null &&
      typeof obj.uuid === 'string' &&
      typeof obj.name === 'string' &&
      typeof obj.email === 'string' &&
      typeof obj.phone === 'string' &&
      obj.role === 'user'
    );
  }
}

export class StaffUser extends FirebaseUser implements IStaffUser {
  isAvailable: boolean;
  ordersAssignedIds: string[];
  role: 'staff' = 'staff';

  constructor(user: Omit<IStaffUser, 'uuid'> & {uuid?: string}) {
    super(user);

    this.isAvailable = user.isAvailable;
    this.ordersAssignedIds = user.ordersAssignedIds;
  }

  static isOfType(obj: any): obj is IStaffUser {
    return (
      obj != null &&
      typeof obj.uuid === 'string' &&
      typeof obj.name === 'string' &&
      typeof obj.email === 'string' &&
      typeof obj.phone === 'string' &&
      typeof obj.isAvailable === 'boolean' &&
      typeof obj.ordersAssignedIds === 'object' &&
      obj.role === 'staff'
    );
  }

  serializeAsJS(): IStaffUser {
    const obj: IStaffUser = {
      ...super.serializeAsJS(),
      role: 'staff',
      isAvailable: this.isAvailable,
      ordersAssignedIds: this.ordersAssignedIds,
    };

    return obj;
  }

  assignOrders(ids: string[]): void {
    this.ordersAssignedIds = Array.from(
      new Set(this.ordersAssignedIds.concat(ids)),
    );

    // TODO Make a better system to handle availability
    // this.isAvailable = false;
  }
}
