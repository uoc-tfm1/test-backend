// Copyright (c) 2021 Irian Montón Beltrán
//
// This Source Code Form is subject to the terms of the Mozilla Public
// License, v. 2.0. If a copy of the MPL was not distributed with this
// file, You can obtain one at http://mozilla.org/MPL/2.0/.

export async function DEBUG_emptyCollection(
  collection: FirebaseFirestore.CollectionReference,
) {
  const docRefs = await collection.listDocuments();

  const promises = docRefs.map((ref) => {
    return ref.delete();
  });

  await Promise.all(promises);
}
