// Copyright (c) 2021 Irian Montón Beltrán
//
// This Source Code Form is subject to the terms of the Mozilla Public
// License, v. 2.0. If a copy of the MPL was not distributed with this
// file, You can obtain one at http://mozilla.org/MPL/2.0/.

class State {
  counter: number = 0;

  increaseCount() {
    this.counter += 1;
  }

  decreaseCount() {
    this.counter = Math.max(0, this.counter - 1);
  }
}

export default State;
